import java.util.Comparator;
import java.util.HashMap;
import java.util.TreeSet;

public class AStar extends PathFinder {

	private class Heuristics {

		private Node to;

		private HashMap<Node, Double> cache = new HashMap<Node, Double>();

		public Heuristics(Node _to) {
			to = _to;
		}

		public double get(Node node) {
			Double heuristic = cache.get(node);

			if (heuristic != null) {
				return heuristic;
			}

			heuristic = node.distanceFrom(to);
			cache.put(node, heuristic);
			return heuristic;
		}

	}

	private Heuristics heuristics;
	private TreeSet<Node> queue;

	public AStar(Graph _graph) {
		super(_graph);
	}

	@Override
	public Path calculateShortestPath(Node from, Node to) {
		guaranteed = new Guaranteed();
		heuristics = new Heuristics(to);
		queue = new TreeSet<Node>(
				Comparator.comparingDouble(node -> guaranteed.get(node).getFirst() + heuristics.get(node)));

		guaranteed.put(from, new Pair<Double, Edge>(0.0, null));
		queue.add(from);

		Node current = null;
		while (current != to) {
			current = queue.pollFirst();
			for (Edge edge : current.getEdges()) {
				Node node = edge.getEnd();
				double distanceThroughCurrent = guaranteed.get(current).getFirst() + edge.getCost();
				if (distanceThroughCurrent < guaranteed.get(node).getFirst()) {
					guaranteed.put(node, new Pair<Double, Edge>(distanceThroughCurrent, edge));
					// To force order update
					queue.remove(node);
					queue.add(node);
				}

			}
		}

		return geteratePathFromGuaranteed(to);
	}

}
