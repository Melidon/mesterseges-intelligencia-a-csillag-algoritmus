import java.util.ArrayList;

public class Path {

	private ArrayList<Edge> edges = new ArrayList<Edge>();

	public void addEdge(Edge edge) {
		if (edges.isEmpty() || edges.get(edges.size() - 1).pointsTo(edge.getStart())) {
			edges.add(edge);
		} else if (edges.get(0).startsAt(edge.getEnd())) {
			edges.add(0, edge);
		} else {
			throw new IllegalArgumentException("You can only add edges that can be attached to one end of the path.");
		}
	}

	public double getLentgh() {
		double length = 0;
		for (Edge edge : edges) {
			length += edge.getCost();
		}
		return length;
	}

}
