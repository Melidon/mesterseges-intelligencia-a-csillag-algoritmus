import java.util.ArrayList;
import java.util.List;

public class Graph {

	private List<Node> nodes = new ArrayList<Node>();
	private List<Edge> edges = new ArrayList<Edge>();

	public void addNode(double x, double y) {
		nodes.add(new Node(new Coordinate(x, y)));
	}

	public Node getNodeById(int id) {
		return nodes.get(id);
	}

	public void addEdgeBetween(int nodeId1, int nodeId2) {
		Node node1 = nodes.get(nodeId1);
		Node node2 = nodes.get(nodeId2);

		Edge edge1 = new Edge(node1, node2);
		Edge edge2 = new Edge(node2, node1);

		node1.addEdge(edge1);
		node2.addEdge(edge2);

		edges.add(edge1);
		edges.add(edge2);
	}

	public double minimumDistance(Node from, Node to) {
		PathFinder pathFinder = new AStar(this);
		Path shortestPath = pathFinder.calculateShortestPath(from, to);
		return shortestPath.getLentgh();
	}

}
