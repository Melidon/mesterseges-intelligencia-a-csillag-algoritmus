
public class Coordinate {

	private double x, y;

	public Coordinate(double _x, double _y) {
		x = _x;
		y = _y;
	}

	public double distanceFrom(Coordinate other) {
		double dx = x-other.x;
		double dy = y-other.y;
		return Math.sqrt(dx * dx + dy * dy);
	}
}
