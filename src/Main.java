import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	private static Graph graph = new Graph();

	private static List<Pair<Integer, Integer>> pairs = new ArrayList<Pair<Integer, Integer>>();

	private static void readInput() {
		Scanner sc = new Scanner(System.in);

		int p = sc.nextInt();
		int n = sc.nextInt();
		int e = sc.nextInt();

		results = new double[p];

		for (int i = 0; i < p; ++i) {
			pairs.add(new Pair<Integer, Integer>(sc.nextInt(), sc.nextInt()));
		}
		for (int i = 0; i < n; ++i) {
			graph.addNode(sc.nextInt(), sc.nextInt());
		}
		for (int i = 0; i < e; ++i) {
			graph.addEdgeBetween(sc.nextInt(), sc.nextInt());
		}

		sc.close();
	}

	private static double[] results;

	private static void calculate() throws InterruptedException {
		List<Thread> queries = new ArrayList<Thread>();

		for (int i = 0; i < pairs.size(); ++i) {
			final int local_i = i;
			queries.add(new Thread(() -> {
				Pair<Integer, Integer> pair = pairs.get(local_i);
				Node from = graph.getNodeById(pair.getFirst());
				Node to = graph.getNodeById(pair.getSecond());
				results[local_i] = graph.minimumDistance(from, to);
			}));
		}

		for (Thread query : queries) {
			query.run();
		}

		for (Thread query : queries) {
			query.join();
		}
	}

	private static void printResults() {
		DecimalFormat formatter = new DecimalFormat("###.00");
		for (double result : results) {
			System.out.print(formatter.format(result) + "\t");
		}
	}

	public static void main(String[] args) throws InterruptedException {
		readInput();
		calculate();
		printResults();
	}

}
