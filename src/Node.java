import java.util.ArrayList;
import java.util.List;

public class Node {

	private Coordinate coordinate;
	private List<Edge> edges = new ArrayList<Edge>();

	public Node(Coordinate _coordinate) {
		coordinate = _coordinate;
	}

	public void addEdge(Edge edge) {
		edges.add(edge);
	}

	public List<Edge> getEdges() {
		return edges;
	}

	public double distanceFrom(Node other) {
		return coordinate.distanceFrom(other.coordinate);
	}

}
