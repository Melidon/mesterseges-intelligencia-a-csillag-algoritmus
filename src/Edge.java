
public class Edge {

	private Node start, end;

	public Edge(Node from, Node to) {
		start = from;
		end = to;
	}

	public Node getStart() {
		return start;
	}

	public Node getEnd() {
		return end;
	}

	public double getCost() {
		return start.distanceFrom(end);
	}

	public boolean startsAt(Node node) {
		return start == node;
	}

	public boolean pointsTo(Node node) {
		return end == node;
	}

}
