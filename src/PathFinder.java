import java.util.HashMap;

public abstract class PathFinder {

	protected Graph graph;

	public PathFinder(Graph _graph) {
		graph = _graph;
	}

	protected class Guaranteed extends HashMap<Node, Pair<Double, Edge>> {

		@Override
		public Pair<Double, Edge> get(Object node) {
			Pair<Double, Edge> guaranteed = super.get(node);
			return guaranteed == null ? new Pair<Double, Edge>(Double.POSITIVE_INFINITY, null) : guaranteed;
		}

	}

	protected Guaranteed guaranteed;

	public abstract Path calculateShortestPath(Node from, Node to);

	protected Path geteratePathFromGuaranteed(Node to) {
		Path shortestPath = new Path();
		for (Edge edge = guaranteed.get(to).getSecond(); edge != null; edge = guaranteed.get(edge.getStart()).getSecond()) {
			shortestPath.addEdge(edge);
		}
		return shortestPath;
	}

}
